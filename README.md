# CSc-335 - Paradigms - Exams

Contains exams (both practice and real ones) from the Fall 2019 CCNY
class CSc-335 (Programming Language Paradigms), taught by Prof. Douglas
Troeger.

As you'd expect from a mid-level undergrad language class, programming
was completed using a functional language (R5RS, which is a dialect of
Scheme) and emphasis was placed on rigorously proving programs that
solve small number theory and data structure challenges.

The class textbooks were:
* Structure and Interpretation of Computer Programs - by Abelson, Sussman, and Sussman
* The Little Schemer - by Friedman, Felleisen, Sussman

### Author, contributors

The solutions were written by Ian S. McBride ([ian-s-mcb][gl].) While
designing the solutions, the author consulted with study partners and
the class professor.

[gl]: https://gitlab.com/ian-s-mcb
