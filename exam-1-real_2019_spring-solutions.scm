; =======================================================================
; CSc-335 - Paradigm - Exam-1 - 2019 Fall
; =======================================================================
; Exam had two question, both regarding the following function:
;
; (similar m n) :: same digits and multiplicity
; Examples
;   1. m = 101,   n = 110,   answer = #t
;   1. m = 40301, n = 30041, answer = #t
;   1. m = 40301, n = 30441, answer = #f
;
; -----------------------------------------------------------------------
; Problem-1 :: iterative using sorting (InsertionSort is recommended)
; -----------------------------------------------------------------------
(define (dec x) (- x 1))
(define (inc x) (+ x 1))
(define (digit-count-iter x)
  (define (iter x i compare)
    (cond ((>= x compare) (iter x (inc i) (* compare 10)))
          (else i)))
  (iter x 1 10))
; Test: (digit-count-iter 305) ; 3
(define (mag d index)
  (cond ((= index 1) d)
        (else (* d (expt 10 (dec index))))))
; Test: (mag 4 3) ; 400
(define (get-digit x index)
  (cond ((= index 1) (modulo x 10))
        (else (truncate (/ (modulo x (expt 10 index)) (expt 10 (dec index)))))))
; Test: (get-digit 305 3) ; 3
(define (set-digit x d index)
  (- (+ x (mag d index)) (mag (get-digit x index) index)))
; Test: (set-digit 305 6 3) ; 605
(define (sort n)
  (let ((c (digit-count-iter n)))
    (define (iter x j i key)
      (cond
        ((and (<= j c) (> i 0) (> (get-digit x i) key))
          (iter (set-digit x (get-digit x i) (inc i)) j (dec i) key))
        ((<= j c)
          (iter (set-digit x key (inc i)) (inc j) j (get-digit x (inc j))))
        (else x)))
    (iter n 2 1 (get-digit n 2))))
; Test: (sort 305) ; 530
(define (similar m n)
  (= (sort m) (sort n)))
; Test: (similar 305 503) ; #t
; Proof of (sort n) by invariant
; - DI
;   * Sort digits by successive insertions of the rightmost unsorted
;     digit, key, into an already sorted subset of digits to the right of
;     the unsorted digits.
;   * Key is correctly inserted into the sorted subset by looping over
;     and shifting forward all sorted digits that belong earlier in the
;     sequence than key.
; - gInv :: a subset of the digits in the number x, from index (j-1) to
;   1, are sorted in descending order
; - Test-1 :: is gInv strong enough to imply correctness of program on
;   termination?
;   * Program terminates when j > c, the number of digits in the number x
;   * By gInv, when j > c, all digits are must be sorted
;   * This test passes
; - Test-2 :: is gInv true the first time the function is called?
;   * First call: (iter n 2 1 (get-digit n 2))
;   * The sorted subset is from index (j-1)=2-1=1 to 1
;   * A subset of digits of length 1 is inherently sorted
; - Test-3 :: if gInv is true for kth call, is it true for (k+1)th call?
;   * For the (k+1)th call, one additional digit is considered for
;     sorting
;   * This additional digit may or may not introduce an inversion
;   * An inversion would initiate a forward shift of digits via the first
;     expression of iter's cond statement
;   * Whether that shift is needed or not, the second expression of
;     iter's cond statement would place the additional digit at the
;     appropriate index, either at the leftmost index (if there were no
;     inversions) or at an interior index (if there was at least one
;     inversion.)
;   * With the additional digit in place, the digits in the number x will
;     be sorted
;   * This test passes

; -----------------------------------------------------------------------
; Problem-2 :: recursive
; TODO complete implementation
; -----------------------------------------------------------------------
(define (digit-count-recur x)
  (cond ((< x 10) 1)
        (else (inc (digit-count (truncate (/ x 10)))))))
