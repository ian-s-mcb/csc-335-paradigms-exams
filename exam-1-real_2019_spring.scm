; =======================================================================
; CSc-335 - Paradigm - Exam-1 - 2019 Fall
; =======================================================================
; Exam had two question, both regarding the following function:
;
; (similar m n) :: same digits and multiplicity
; Examples
;   1. m = 101,   n = 110,   answer = #t
;   1. m = 40301, n = 30041, answer = #t
;   1. m = 40301, n = 30441, answer = #f
;
; -----------------------------------------------------------------------
; Problem-1 :: iterative using sorting (InsertionSort is recommended)
; -----------------------------------------------------------------------

; -----------------------------------------------------------------------
; Problem-2 :: recursive
; TODO complete implementation
; -----------------------------------------------------------------------
